import 'package:flutter/material.dart';

class Payment extends StatelessWidget {
  const Payment({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Center(child: Text("payment Page",style: TextStyle(color: Colors.black))),
          elevation: 0.0,
          backgroundColor: Colors.transparent),
      body: Column(children: [
        Padding(
          padding: const EdgeInsets.all(50.0),
          child: Center(child: Text("Payment successful........",style: TextStyle(fontSize: 40),)),
        ),
        ElevatedButton(onPressed: () {}, child: Text("go to home"))
      ]),
    );
  }
}
