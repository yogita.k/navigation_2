import 'package:flutter/material.dart';

class Details extends StatefulWidget {
  const Details({Key? key}) : super(key: key);

  @override
  State<Details> createState() => _DetailsState();
}

class _DetailsState extends State<Details> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            elevation: 0.0,
            backgroundColor: Colors.transparent,
            title: Center(
                child: Text(
              "detail about product",
              style: TextStyle(color: Colors.black),
            ))),
        body: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: Container(
                height: 400,
                color: Colors.grey,
                child: Center(
                  child: Text("--------details--------",
                      style: TextStyle(fontSize: 30)),
                ),
              ),
            ),
            ElevatedButton(
              onPressed: () {},
              child: Text("Go to cart", style: TextStyle(fontSize: 30)),
              style: ButtonStyle(
                  backgroundColor:
                      MaterialStatePropertyAll(Colors.greenAccent)),
            ),
            ElevatedButton(
              onPressed: () {},
              child: Text("back to homescreen", style: TextStyle(fontSize: 30)),
              style: ButtonStyle(
                  backgroundColor:
                      MaterialStatePropertyAll(Colors.greenAccent)),
            )
          ],
        ));
  }
}
