import 'package:flutter/material.dart';

class Cart extends StatefulWidget {
  const Cart({Key? key}) : super(key: key);

  @override
  State<Cart> createState() => _CartState();
}

class _CartState extends State<Cart> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          elevation: 0.0,
          backgroundColor: Colors.transparent,
          title: Center(
              child: Text(
            "CartScreen",
            style: TextStyle(color: Colors.black),
          ))),
      body: Column(children: [
        Container(
            height: 500,
            child: Center(
                child: Icon(
              Icons.shopping_cart,
              size: 250,
            ))),
        ElevatedButton(
          onPressed: () {},
          child: Text("go for payment"),
          style: ButtonStyle(backgroundColor: MaterialStatePropertyAll(Colors.blueAccent)),
        ),
        ElevatedButton(
          onPressed: () {},
          child: Text("back to detailscreen"),
          style: ButtonStyle(backgroundColor: MaterialStatePropertyAll(Colors.blueAccent)),
        )
      ]),
    );
  }
}
