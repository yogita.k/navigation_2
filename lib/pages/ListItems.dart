import 'package:flutter/material.dart';

class ListItem extends StatefulWidget {
  const ListItem({Key? key}) : super(key: key);

  @override
  State<ListItem> createState() => _ListItemState();
}

class _ListItemState extends State<ListItem> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          elevation: 0.0,
          backgroundColor: Colors.transparent,
          title: Text("List Of Items", style: TextStyle(color: Colors.black)),
          actions: [
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: Icon(Icons.shopping_cart, color: Colors.black),
            )
          ]),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: Container(
                height: 500,
                color: Colors.purpleAccent,
                child: Center(
                    child: Text(
                  "item ",
                  style: TextStyle(fontSize: 25),
                ))),
          ),
          ElevatedButton(
              style:
                  ButtonStyle(backgroundColor: MaterialStatePropertyAll(Colors.purpleAccent)),
              onPressed: () {},
              child: Text("Go detail Ppage"))
        ],
      ),
    );
  }
}
